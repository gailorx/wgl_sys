extern crate winapi;

pub mod extension {
	pub mod types {

		use super::super::types::{HDC, HGLRC};
		use winapi::{UINT, FLOAT, BOOL, c_int, c_char};

		pub type GetExtensionStringArb = unsafe extern "stdcall" fn(HDC) -> *const c_char;
		pub type GetExtensionStringExt = unsafe extern "stdcall" fn() -> *const c_char;
		pub type CreateContextAttribsArb = unsafe extern "stdcall" fn(HDC, HGLRC, *const c_int) -> HGLRC;
		pub type ChoosePixelFormatArb = unsafe extern "stdcall" fn(HDC, *const c_int, *const FLOAT, UINT, *mut c_int, *mut UINT) -> BOOL;
		pub type SwapIntervalExt = unsafe extern "stdcall" fn(c_int) -> BOOL;
	}
	pub mod names {

		pub const CREATE_CONTEXT_ARB: &'static str = "WGL_ARB_create_context";
		pub const CREATE_CONTEXT_PROFILE_ARB: &'static str = "WGL_ARB_create_context_profile";
		pub const CREATE_CONTEXT_ROBUSTNESS_ARB: &'static str = "WGL_ARB_create_context_robustness";
		pub const GET_EXTENSION_STRING_ARB: &'static str = "WGL_ARB_extensions_string";
		pub const GET_EXTENSION_STRING_EXT: &'static str = "WGL_EXT_extensions_string";
		pub const FRAMEBUFFER_SRGB_ARB: &'static str = "WGL_ARB_framebuffer_sRGB";
		pub const MULTISAMPLE_ARB: &'static str = "WGL_ARB_multisample";
		pub const CHOOSE_PIXEL_FORMAT_ARB: &'static str = "WGL_ARB_pixel_format";
		pub const SWAP_CONTROL_EXT: &'static str = "WGL_EXT_swap_control";
		pub const SWAP_CONTROL_TEAR_EXT: &'static str = "WGL_EXT_swap_control_tear";
	}

	pub mod constants {
		use super::super::types::GLint;

		// Context arb //
		pub const CONTEXT_MAJOR_VERSION_ARB: GLint = 0x2091;
		pub const CONTEXT_MINOR_VERSION_ARB: GLint = 0x2092;
		pub const CONTEXT_LAYER_PLANE_ARB: GLint = 0x2093;
		pub const CONTEXT_FLAGS_ARB: GLint = 0x2094;
		pub const CONTEXT_PROFILE_MASK_ARB: GLint = 0x9126;
		pub const CONTEXT_DEBUG_BIT_ARB: GLint = 0x0001;
		pub const CONTEXT_FORWARD_COMPATIBLE_BIT_ARB: GLint = 0x0002;
		pub const CONTEXT_CORE_PROFILE_BIT_ARB: GLint = 0x00000001;
		pub const CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB: GLint = 0x00000002;

		// Pixel format arb //
		pub const NUMBER_PIXEL_FORMATS_ARB: GLint = 0x2000;
		pub const DRAW_TO_WINDOW_ARB: GLint = 0x2001;
		pub const DRAW_TO_BITMAP_ARB: GLint = 0x2002;
		pub const ACCELERATION_ARB: GLint = 0x2003;
		pub const NEED_PALETTE_ARB: GLint = 0x2004;
		pub const NEED_SYSTEM_PALETTE_ARB: GLint = 0x2005;
		pub const SWAP_LAYER_BUFFERS_ARB: GLint = 0x2006;
		pub const SWAP_METHOD_ARB: GLint = 0x2007;
		pub const NUMBER_OVERLAYS_ARB: GLint = 0x2008;
		pub const NUMBER_UNDERLAYS_ARB: GLint = 0x2009;
		pub const TRANSPARENT_ARB: GLint = 0x200A;
		pub const TRANSPARENT_RED_VALUE_ARB: GLint = 0x2037;
		pub const TRANSPARENT_GREEN_VALUE_ARB: GLint = 0x2038;
		pub const TRANSPARENT_BLUE_VALUE_ARB: GLint = 0x2039;
		pub const TRANSPARENT_ALPHA_VALUE_ARB: GLint = 0x203A;
		pub const TRANSPARENT_INDEX_VALUE_ARB: GLint = 0x203B;
		pub const SHARE_DEPTH_ARB: GLint = 0x200C;
		pub const SHARE_STENCIL_ARB: GLint = 0x200D;
		pub const SHARE_ACCUM_ARB: GLint = 0x200E;
		pub const SUPPORT_GDI_ARB: GLint = 0x200F;
		pub const SUPPORT_OPENGL_ARB: GLint = 0x2010;
		pub const DOUBLE_BUFFER_ARB: GLint = 0x2011;
		pub const STEREO_ARB: GLint = 0x2012;
		pub const PIXEL_TYPE_ARB: GLint = 0x2013;
		pub const COLOR_BITS_ARB: GLint = 0x2014;
		pub const RED_BITS_ARB: GLint = 0x2015;
		pub const RED_SHIFT_ARB: GLint = 0x2016;
		pub const GREEN_BITS_ARB: GLint = 0x2017;
		pub const GREEN_SHIFT_ARB: GLint = 0x2018;
		pub const BLUE_BITS_ARB: GLint = 0x2019;
		pub const BLUE_SHIFT_ARB: GLint = 0x201A;
		pub const ALPHA_BITS_ARB: GLint = 0x201B;
		pub const ALPHA_SHIFT_ARB: GLint = 0x201C;
		pub const ACCUM_BITS_ARB: GLint = 0x201D;
		pub const ACCUM_RED_BITS_ARB: GLint = 0x201E;
		pub const ACCUM_GREEN_BITS_ARB: GLint = 0x201F;
		pub const ACCUM_BLUE_BITS_ARB: GLint = 0x2020;
		pub const ACCUM_ALPHA_BITS_ARB: GLint = 0x2021;
		pub const DEPTH_BITS_ARB: GLint = 0x2022;
		pub const STENCIL_BITS_ARB: GLint = 0x2023;
		pub const AUX_BUFFERS_ARB: GLint = 0x2024;
		pub const NO_ACCELERATION_ARB: GLint = 0x2025;
		pub const GENERIC_ACCELERATION_ARB: GLint = 0x2026;
		pub const FULL_ACCELERATION_ARB: GLint = 0x2027;

		pub const SWAP_EXCHANGE_ARB: GLint = 0x2028;
		pub const SWAP_COPY_ARB: GLint = 0x2029;
		pub const SWAP_UNDEFINED_ARB: GLint = 0x202A;

		pub const TYPE_RGBA_ARB: GLint = 0x202B;
		pub const TYPE_COLORINDEX_ARB: GLint = 0x202C;

		pub const SAMPLE_BUFFERS_ARB: GLint = 0x2041;
		pub const SAMPLES_ARB: GLint = 0x2042;

		pub const FRAMEBUFFER_SRGB_CAPABLE_ARB: GLint = 0x20A9;
	}
}

#[link(name = "opengl32")] extern {}
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
